#include "reference_computation.h"
#include "cuda_helper.h"

#include <mkl.h>
#include <cublas_v2.h>

#include <stdio.h>

#define NUMERICAL_THRESHOLD 1e-12

namespace ComputationValidation
{
    bool AT_B_A_validate(const double * A, const double * B,
                         const double * C_computed,
                         const int M)
    {
        double * C_reference = (double *) malloc(M * M * sizeof(double));
        double * work = (double *) malloc(M * M * sizeof(double));

        char transa = 'T', transb = 'N';
        double alpha = 1.0, beta = 0.0;
        dgemm( // Compute C = alpha * A * B + beta * C
            &transa, // If transpose A ('T') or not ('N')
            &transb, // If transpose B
            &M, // M
            &M, // N
            &M, // K
            &alpha,
            A,
            &M, // LDA, the number of elements between the first element of i-th column to that of (i+1)-th column
            B,
            &M, // LDB
            &beta,
            work,
            &M // LDC
        );

        transa = 'N'; transb = 'N';
        alpha = 1.0; beta = 0.0;
        dgemm(
            &transa,
            &transb,
            &M,
            &M,
            &M,
            &alpha,
            work,
            &M,
            A,
            &M,
            &beta,
            C_reference,
            &M
        );

        int incorrect_count = 0;
        for (int i = 0; i < M * M; i++)
            if (fabs(C_computed[i] - C_reference[i]) > NUMERICAL_THRESHOLD)
                incorrect_count++;

        free(C_reference);
        free(work);

        if (incorrect_count > 0)
        {
            printf("Your result doesn't match reference computation! There're %d points different.\n",
                incorrect_count);
            return false;
        }
        else
        {
            printf("Test passed, good job!\n");
            return true;
        }
    }
}

namespace OptimizedComputation
{
    void AT_B_A_optimized_cpu(const double * A, const double * B,
                              double * work, double * C,
                              const int M)
    {
        char transa = 'T', transb = 'N';
        double alpha = 1.0, beta = 0.0;
        dgemm(
            &transa,
            &transb,
            &M,
            &M,
            &M,
            &alpha,
            A,
            &M,
            B,
            &M,
            &beta,
            work,
            &M
        );

        transa = 'N';
        dgemm(
            &transa,
            &transb,
            &M,
            &M,
            &M,
            &alpha,
            work,
            &M,
            A,
            &M,
            &beta,
            C,
            &M
        );
    }

    void AT_B_A_optimized_gpu(const double * d_A, const double * d_B,
                              double * d_work, double * d_C,
                              const int M)
    {
        cublasHandle_t handle;
        cublasStatus_t stat;
        cudaError_t cudaStat;
        stat = cublasCreate(&handle);
        CUBLASERR(stat);
        
        double alpha = 1.0, beta = 0.0;
        stat = cublasDgemm(
            handle,
            CUBLAS_OP_T,
            CUBLAS_OP_N,
            M,
            M,
            M,
            &alpha,
            d_A,
            M,
            d_B,
            M,
            &beta,
            d_work,
            M
        );
        CUBLASERR(stat);

        stat = cublasDgemm(
            handle,
            CUBLAS_OP_N,
            CUBLAS_OP_N,
            M,
            M,
            M,
            &alpha,
            d_work,
            M,
            d_A,
            M,
            &beta,
            d_C,
            M
        );
        CUBLASERR(stat);
    }
}