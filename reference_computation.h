#ifndef REFERENCE_COMPUTATION_H_
#define REFERENCE_COMPUTATION_H_

/**
 * Functions to validate the matrix computation.
 **/
namespace ComputationValidation
{
    /**
     * \brief Compute A^T*B*A using LAPACK function calls, and
     *        compare C_compute against the LAPACK result.
     * 
     * @param A         Matrix A with dimension M * M.
     * @param B         Matrix B with dimension M * M.
     * @param C_compute The C = A^T*B*A computed with your algorithm, with dimension M * M.
     * @param M         The first dimension of square matrix A, B or C.
     * @return          Ture if the numerical error for each C entry is smaller than a threshold
     *                  (10^-14).
     **/
    bool AT_B_A_validate(const double * A, const double * B,
                         const double * C_computed,
                         const int M);
}

/**
 * Optimized functions for A^T*B*A computation.
 **/
namespace OptimizedComputation
{
    /**
     * \brief Compute A^T*B*A using LAPACK function from intel MKL library.
     *        That's a reference implementation for matrix operations on CPU.
     * 
     * @param A         Matrix A with dimension M * M.
     * @param B         Matrix B with dimension M * M.
     * @param work      Memory space of M * M doubles for stratch.
     * @param C         Output C = A^T*B*A, with dimension M * M.
     * @param M         The first dimension of square matrix A, B or C.
     **/
    void AT_B_A_optimized_cpu(const double * A, const double * B,
                              double * work, double * C,
                              const int M);

    /**
     * \brief Compute A^T*B*A using CUBLAS functions.
     *        That's a reference implementation for matrix operations on GPU.
     *        All matrices and work space are on GPU memory.
     * 
     * @param d_A       Matrix A with dimension M * M.
     * @param d_B       Matrix B with dimension M * M.
     * @param d_work    Memory space of M * M doubles for stratch.
     * @param d_C       Output C = A^T*B*A, with dimension M * M.
     * @param M         The first dimension of square matrix A, B or C.
     **/
    void AT_B_A_optimized_gpu(const double * d_A, const double * d_B,
                              double * d_work, double * d_C,
                              const int M);
}

#endif