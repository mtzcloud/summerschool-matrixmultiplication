#include "matrix_helper.h"

#include <stdio.h>

namespace VectorHelper
{
    void print_vector(const int N, const double * const vector, const char * const name)
    {
        printf("%s = [", name);
        for (int i = 0; i < N; i++)
        {
            printf("%.2f, ", vector[i]);
        }
        printf("]\n");
    }

    void generate_vector(const int N, double * A)
    {
        for (int i = 0; i < N; i++)
        {
            A[i] = (double)i; // Typecasting int as a double
        }
    }
}

namespace MatrixReader
{
    bool get_matrix_dimension(const char * const path, int * M, int * N, int * K)
    {
        FILE* fp = fopen(path, "rb");
        if (fp == NULL)
        {
            printf("Cannot open %s!\n", path);
            return false;
        }
        fread(M, sizeof(int), 1, fp);
        fread(N, sizeof(int), 1, fp);
        fread(K, sizeof(int), 1, fp);
        fclose(fp);

        return true;
    }

    bool get_matrix_data(const char * const path, double * A, double * B)
    {
        FILE* fp = fopen(path, "rb");
        if (fp == NULL)
        {
            printf("Cannot open %s!\n", path);
            return false;
        }
        int M, N, K;
        fread(&M, sizeof(int), 1, fp);
        fread(&N, sizeof(int), 1, fp);
        fread(&K, sizeof(int), 1, fp);
        fread(A, sizeof(double), M * K, fp);
        fread(B, sizeof(double), K * N, fp);
        fclose(fp);

        return true;
    }
}

namespace MatrixPrinter
{
    void print_matrix(const int M, const int N, const double * const matrix, const char * const name)
    {
        printf("%s = np.array([\n", name);
        for (int i = 0; i < M; i++)
        {
            printf("[");
            for (int j = 0; j < N; j++)
                printf("%.5f, ", matrix[i + j * M]);
            printf("],\n");
        }
        printf("])\n");
    }
}

