#ifndef MATRIX_HELPER_H_
#define MATRIX_HELPER_H_

/**
 * \brief Functions for vectors of type double
 **/
namespace VectorHelper
{
    /**
     * \brief Prints a vector of doubles
     *
     * @param N      size of vector
     * @param vector array of doubles to be printed
     * @param name   name for array to be printed
     **/
    void print_vector(const int N, const double * const vector, const char * const name);

    /**
     * \brief Generates a vector of doubles where each element value is equal to its index
     *
     * @param N  size of vector
     * @param A  pointer to array to store values
     **/
    void generate_vector(const int N, double * A);
}

/**
 * \brief Functions for reading the matrices provided by the teaching team.
 **/
namespace MatrixReader
{
    /**
     * \brief Get the dimension of matrix A (M * K) and B (K * N).
     *        These values inform you the size of memory space you need to allocate.
     *        The reason why we're accepting the pointers of M, N and K, instead of
     *        passing them by value, is that in the later case we're not able to
     *        modify the memory of the caller inside this function.
     *
     * @param path  The path of the data file.
     * @param M     Output the first dimension of A.
     * @param N     Output the second dimension of B.
     * @param K     Output the second dimension of A (also the first dimension of B).
     * @return      Return true is the file is read successfully.
     **/
    bool get_matrix_dimension(const char * const path, int * M, int * N, int * K);

    /**
     * \brief Get the data of matrix A and B. 
     * 
     * @param path  The path of the data file.
     * @param A     Output the content of A of dimension M * K, in column major.
     * @param B     Output the content of B of dimension K * N, in column major.
     * @return      Return true is the file is read successfully.
     **/
    bool get_matrix_data(const char * const path, double * A, double * B);
}

/**
 * \brief Functions for printing the matrices in a nice-looking format.
 **/
namespace MatrixPrinter
{
    /**
     * \brief Print the matrix in python numpy input format. Don't print matrices
     *        larger than 32*32 to command prompt.
     * 
     * @param M         The first dimension of the matrix.
     * @param N         The second dimension of the matrix.
     * @param matrix    The matrix to print.
     * @param name      The name of the matrix, will get printed as the variable name.
     **/
    void print_matrix(const int M, const int N, const double * const matrix, const char * const name);
}

#endif
