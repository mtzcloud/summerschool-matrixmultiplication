#ifndef MATRIX_OPERATOR_H_
#define MATRIX_OPERATOR_H_

/**
 * Functions that performs matrix operations
 **/
namespace MatrixOperator
{
    /**
     * \brief TODO: perform matrix multiplication C = AB.
     *
     * @param A     Matrix A with dimension M * K.
     * @param B     Matrix B with dimension K * N.
     * @param C     Matrix C with dimension M * N, will get overwritten.
     * @param M     The first dimension of A (also the first dimension of C).
     * @param N     The second dimension of B (also the second dimension of C).
     * @param K     The second dimension of A (also the first dimension of B).
     **/
    void matrix_multiply_cpu(const double * A, const double * B,
                             double * C,
                             const int M, const int N, const int K);

    /**
     * \brief TODO: perform matrix multiplication C = AB.
     *        All matrices are on GPU memory.
     *
     * @param d_A   Matrix A with dimension M * K.
     * @param d_B   Matrix B with dimension K * N.
     * @param d_C   Matrix C with dimension M * N, will get overwritten.
     * @param M     The first dimension of A (also the first dimension of C).
     * @param N     The second dimension of B (also the second dimension of C).
     * @param K     The second dimension of A (also the first dimension of B).
     **/
    void matrix_multiply_gpu(const double * d_A, const double * d_B,
                             double * d_C,
                             const int M, const int N, const int K);

    /**
     * \brief TODO: Perform an out-of-place matric transpose operation A = A^T.
     *
     * @param from  Matrix A with dimension M * N.
     * @param to    Output A^T with dimension N * M.
     * @param M     The first dimension of A.
     * @param N     The second dimension of A.
     **/
    void matrix_transpose_outofplace_cpu(const double * from, double * to,
                                         const int M, const int N);

    /**
     * \brief TODO: Perform an out-of-place matric transpose operation A = A^T.
     *        All matrices are on GPU memory.
     *
     * @param d_from    Matrix A with dimension M * N.
     * @param d_to      Output A^T with dimension N * M.
     * @param M         The first dimension of A.
     * @param N         The second dimension of A.
     **/
    void matrix_transpose_outofplace_gpu(const double * d_from, double * d_to,
                                         const int M, const int N);
}

#endif