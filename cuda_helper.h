#pragma once

#include <stdio.h>
#include <stdlib.h>

/**
 * \brief Functions to check the return value of cuda and cublas function calls.
 *        If an error is detected, they'll print the error and terminate the program.
 *        They come from cuda examples and tensorbox.h, and they have particular style
 *        that you should NEVER use.
 */

template <typename T>
void check(T result, char const *const func, const char *const file,
           int const line) {
  if (result) {
    fprintf(stderr, "CUDA error at %s:%d code=%d(%s) \"%s\" \n", file, line,
            static_cast<unsigned int>(result), cudaGetErrorName(result), func);
    exit(EXIT_FAILURE);
  }
}

// This will output the proper CUDA error strings in the event
// that a CUDA host call returns an error
#define checkCudaErrors(val) check((val), #val, __FILE__, __LINE__)


/*! \def CUBLASERR
  Check if there has been a CUBLAS error. If so, report and die.
*/
#define CUBLASERR(STATUS) { if (STATUS != CUBLAS_STATUS_SUCCESS) {	\
  if (STATUS == CUBLAS_STATUS_NOT_INITIALIZED) {			\
    printf("CUBLAS error: CUBLAS_STATUS_NOT_INITIALIZED, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_ALLOC_FAILED) {			\
    printf("CUBLAS error: CUBLAS_STATUS_ALLOC_FAILED, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_INVALID_VALUE) {			\
    printf("CUBLAS error: CUBLAS_STATUS_INVALID_VALUE, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_ARCH_MISMATCH) {			\
  printf("CUBLAS error: CUBLAS_STATUS_ARCH_MISMATCH, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_MAPPING_ERROR) {			\
    printf("CUBLAS error: CUBLAS_STATUS_MAPPING_ERROR, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_EXECUTION_FAILED) {			\
    printf("CUBLAS error: CUBLAS_STATUS_EXECUTION_FAILED, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_INTERNAL_ERROR) {			\
    printf("CUBLAS error: CUBLAS_STATUS_INTERNAL_ERROR, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_NOT_SUPPORTED) {			\
    printf("CUBLAS error: CUBLAS_STATUS_NOT_SUPPORTED, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  else if (STATUS == CUBLAS_STATUS_LICENSE_ERROR) {			\
    printf("CUBLAS error: CUBLAS_STATUS_LICENSE_ERROR, file %s, line %d\n", __FILE__, __LINE__); \
  }									\
  exit(1);          \
}}
  