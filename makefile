# Note: If you see a tab in this file, it is necessary, don't change it to space.

# The following code defines a set of variables used later in makefile,
# which specifies the compiler and compilef / linker flags we're using.
ICPC := icpc
NVCC := nvcc
CPPFLAGS := -std=c++11 -static-intel -O2 -g
# TODO: Check if the architecture (the number after compute_ and sm_) of your GPU is included
# in nvcc compiler flags. You can find the architecture of specific GPU model from
# https://arnon.dk/matching-sm-architectures-arch-and-gencode-for-various-nvidia-cards/
# if not, add another -gencode flag for your GPU.
NVCUFLAGS := -O2 --generate-line-info -gencode=arch=compute_52,code=sm_52 -gencode=arch=compute_61,code=sm_61
LDFLAGS := -static-intel -lcuda -lcudart -lcublas -mkl

# Defines the build directory. We don't want our executable and temporary files from
# compilation process mixed with our source code.
# Usually there's a "src/" folder for source codes and "header/" folder for header files,
# but Henry is lazy so we just put them in root folder :)
BUILDDIR := ./build
OBJDIR := $(BUILDDIR)/obj
TARGETDIR := $(BUILDDIR)/bin

# All the source code files. We don't need to include the header files,
# because they're "copied to" each source file including them.
# TODO: when you add a new .cpp, .cu, (.c, .f, .f90) file, you need to append
# the file path here. '\' without anything afterward means line continuation.
SRC := main.cpp \
       matrix_helper.cpp \
	   reference_computation.cpp

# The following code reads: For every substring in SRC, first find the ones matching
# %.cu pattern. '%' is a pattern that match any string, so that basically find all
# cuda source code. Then, match them with %.cu expression, and replace it with
# $(OBJDIR)/%.o, keeping the pattern that '%' is refering to.
# Then, do the same for %.cpp files, and combine the two lists we got.
# We are going to generate these .o files and link them together to get our executable.
OBJ := $(patsubst %.cu,  $(OBJDIR)/%.o, $(filter %.cu,  $(SRC))) \
       $(patsubst %.cpp, $(OBJDIR)/%.o, $(filter %.cpp, $(SRC)))

# The following line defines our first target "default". The first target is
# what a "make" command will generate by default, except if ".DEFAULT_GOAL" is defined.
# After ':' is the prerequisites of "default", which is a file called "matrix_multiply".
# So make process will try to find the rules later in this file that can
# generate "matrix_multiply".
default : $(TARGETDIR)/matrix_multiply

# The following line defines another target "clean". It doesn't depend on anything,
# and usually nothing depend on "clean", so it'll not get triggered by default.
# If you want to run this target, you need to run the command "make clean".
# And all what this target does is to run the "rm" command, which remove all the files
# generated in the normal make process.
clean :
	rm -f $(TARGETDIR)/* $(OBJDIR)/*

# The following code compiles c++ source codes (.cpp) into object files (.o).
# An object file is generated after compilation of single source file,
# and before linkage to other object files and libraries.
# The first line defines a general rule, where we generate target object files in OBJDIR folder,
# based on its prerequisites on the right.
# In order to generate these object files, the next few lines are executed.
# They're basically bash scripts. 
# The '@' at the beginning tells the terminal not to print the command itself.
# "$@" refers to the target ($(OBJDIR)/%.o), "$<" refers to the first prerequisite (%.cpp),
# other predefined variables are available from
# http://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables
# The last line of this code segment is the actual compilation code.
# ICPC is our intel compiler, similar to g++. CPPFLAGS are passed to ICPC as compiler flags.
# "-c" tell the compiler to generate object files without linkage.
# An input not beginning with '-' is treated as input files ($<).
# "-o" specifies the output file name ($@).
$(OBJDIR)/%.o : %.cpp
	@printf "Compiling %-25s > %-25s\n" $< $@
	@mkdir -p $(dir $@)
	@$(ICPC) -c $(CPPFLAGS) $< -o $@

# TODO: Write a rule to compile cuda source files (.cu) into object files.
# It should use NVCC as compiler, and have NVCUFLAGS as additional compiler input.


# The following code generate the executable, called matrix_multiply.
# Now we don't have "-c" flag for the compiler, and we add in linker flags (LDFLAGS).
# ICPC will perform the linkage process, which links in all the OBJ files, as well as
# CUDA libraries, via flags like "-lcublas" in LDFLAGS. That specific flag reads:
# link to ("-l") libcublas.a ("lib" and ".a" are added to "cublas" string,
# ".a" means it's a static library, we can generate our own using "-fpic -staticlib" flags).
# We're trying to perform static linkage, which provides a stand-alone executable,
# which means the binary itself is enough for execution, no dynamic libraries (.so) are needed.
# Usually you'll get a warning that something is linked in dynamically, so the corresponding
# dynamic libraries have to be in the $PATH environment for the executable to run properly.
$(TARGETDIR)/matrix_multiply : $(OBJ)
	@printf "Building %-25s\n" $< $@
	@mkdir -p $(dir $@)
	@$(ICPC) $(OBJ) $(LDFLAGS) -o $@